<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{poll}', "PollController@show")->name("showPoll");
Route::post('/vote', "PollController@vote")->name("vote");

Route::get('/poll/create',"PollController@create")->name("create");
Route::get('/poll/update/{poll}',"PollController@update")->name("update");
Route::post('/poll/store',"PollController@store")->name("store");
Route::post('/poll/store/{poll?}',"PollController@store")->name("store");
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\Business\PollBusiness;

class PollController extends Controller
{
    public function show(Poll $poll)
   	{
   		return view("poll.show",[
   			'poll' => $poll
   		]);
   	}

   	public function vote(Request $request)
   	{
   		return (new PollBusiness())->vote($request);
   	}

      public function create()
      {
         return (new PollBusiness())->createForm();
      }

      public function store($poll = null, Request $request)
      {
            return(new PollBusiness())->store($poll,$request);
      }
      public function update(Poll $poll)
      {
         
         return (new PollBusiness())->updateForm($poll);
      }

}

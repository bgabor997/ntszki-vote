<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vote;
use App\Poll;
class PollOption extends Model
{
    public function votes()
    {
    	return $this->hasMany(Vote::class,"option_id","id");
    }
    public function poll()
    {
    	return $this->belongsTo(Poll::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PollOption;
use Carbon\Carbon;

class Poll extends Model
{
    public function options()
    {
    	return $this->hasMany(PollOption::class);
    }

    public function canVote($ip)
    {
    	$expires = Carbon::parse($this->expires);
    	if(Carbon::now()->greaterThan($expires))
    		return false;
    	foreach($this->options as $option){
    		foreach($option->votes as $vote){
    			if($vote->ip == $ip)
    				return false;
    		}
    	}
    	return true;
    }
}

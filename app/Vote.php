<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PollOption;

class Vote extends Model
{
    public function option()
    {
    	return $this->belongsTo(PollOption::class,"option_id","id");
    }
}

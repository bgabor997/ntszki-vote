<?php

namespace App\Business;

use Illuminate\Http\Request;
use App\Poll;
use App\PollOption;
use App\Vote;
use Carbon\Carbon;

class PollBusiness extends BaseBusiness
{
		public function vote(Request $request)
		{
			$poll = Poll::find($request->poll_id);
			if($poll->canVote($request->ip())){
				if($poll->multiple){
					foreach ($request->votes as $vote) {
						$v = new Vote();
						$option = PollOption::find($vote);
						$v->ip = $request->ip();
						$v->option()->associate($option);
						$v->save();
					}
				}
				else{
					$v = new Vote();
					$option = PollOption::find($request->vote);
					$v->ip = $request->ip();
					$v->option()->associate($option);
					$v->save();
				}
			}
			return redirect()->route("showPoll",$poll->id);

		}

		public function createForm()
		{
			return view("poll.create");
		}

		public function store($poll, Request $request)
		{
			$p;
			$method = "create";
			if ($poll == null) {
				$p= new poll;
			}else{
				$p=poll::find($poll);
				$method = "update";
			}
			$p->question = $request->question;
			$p->expires = Carbon::now()->addweek();
			$p->multiple = $request->multiple;
			$p->save();
		
			if($method == "create")
				foreach($request->answer as $answer) {
					if ($answer != null) {
				         $a = new PollOption();
				         $a->name = $answer;
				         $a->poll()->associate($p);
				         $a->save();
			     	}
			    }
		   	return redirect()->route("showPoll",$p->id);
		}

		public function updateForm(Poll $poll)
		{
			return view("poll.update",["poll"=>$poll]);
		}
}
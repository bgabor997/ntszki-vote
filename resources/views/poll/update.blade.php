<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<form method="POST" action="{{ route('store',$poll->id) }}">
	@csrf
	<p>
	kérdés
	<input type="text" name="question" value="{{ $poll->question }}">
	</p>
	<p>
		egy
		<input type="radio" name="multiple" value="0" @if($poll->multiple == 0) checked @endif>
		több
		<input type="radio" name="multiple" value="1" @if($poll->multiple == 1) checked @endif>
	</p>
	@foreach($poll->options as $key => $option)
	<p>
			válasz {{ $key+1}}
		<input type="text" name="answer[]" value="{{ $option->name }}">
	</p>
	@endforeach
	<p>
		<input type="submit" name="létrehozás">
	</p>
	
</form>
</body>
</html>
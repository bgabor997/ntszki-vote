<!DOCTYPE html>
<html>
<head>
	<title>Szavazás</title>
</head>
<body>
	<p>{{ $poll->question }} <strong>(Lejárat: {{ $poll->expires }})</strong></p>
	@if($poll->canVote(Request::ip()))
	<form method="POST" action="{{ route('vote') }}">
		@endif
		@csrf
		<input type="hidden" value="{{ $poll->id }}" name="poll_id">
		<ul>
			@foreach($poll->options as $option)
				<li>
					@if($poll->canVote(Request::ip()))
						@if($poll->multiple == 0)
							<input type="radio" value="{{ $option->id }}" name="vote">
						@else
							<input type="checkbox" value="{{ $option->id }}" name="votes[]">	
						@endif
					@endif
					{{ $option->name }} ( {{ count($option->votes) }})
				</li>
			@endforeach
		</ul>
		@if($poll->canVote(Request::ip()))
			<input type="submit" value="Szvazás!">
		</form>
		@endif
</body>
</html>